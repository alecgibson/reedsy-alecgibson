# Question 1

## Question

Tell us about one of your commercial projects with Node.js and/or AngularJS.

## Introduction

I spent a year working on a project for BBC Monitoring. BBC Monitoring is an 
offshoot of the BBC proper, and are situated in Reading (although they have
plans to move to London). 

They have [roots with GCHQ reaching back to World War II][BBC History], where
employees were tasked with listening to and analysing foreign news. Today, BBC
Monitoring still has some interaction with GCHQ, but funding cuts have led to
a need to [focus more on commerciality][how Monitoring is changing] and a 
revamp of their outdated 90's user portal.

[BBC History]: http://news.bbc.co.uk/aboutbbcnews/spl/hi/history/noflash/html/1930s.stm
[how Monitoring is changing]: https://publications.parliament.uk/pa/cm201617/cmselect/cmfaff/732/73205.htm#_idTextAnchor014

## Preview

The new BBC Monitoring portal can be previewed [here][Monitoring preview], and
a tour of its features can be seen [here][Monitoring tour].

[Monitoring preview]: https://monitoring.bbc.co.uk/?preview=true
[Monitoring tour]: https://monitoring.bbc.co.uk/guided-tour

## Softwire's work

Softwire - the company I currently work for - won a bid to completely rebuild
a brand new user portal from the ground up. The site deals with millions of
news documents, which are imported in a constant stream from BBC editors. The
documents are fully searchable, and a key feature is the ability to set up 
filters for this constant news stream to find international news on whatever
niche you may be interested. These filters can also send email alerts in live
time (or in daily/weekly digests).

## Tech stack

The Monitoring portal is built on a stateless Java 1.8 backend running 
[Dropwizard][Dropwizard]. The frontend was built on Angular 1.4, with 
[UI Router][UI Router] performing the routing.

The documents were stored
in [Elasticsearch][Elasticsearch], which provided solid search and 
versioning capabilities right out of the box, and was capable of dealing
with the millions of documents we had to store and search.

All other data (user data, saved filters, etc.) was persisted in a 
[MySQL][MySQL] database.

Hosting was provided by [AWS][AWS].

[Dropwizard]: http://www.dropwizard.io/1.1.4/docs/ 
[UI Router]: https://ui-router.github.io/ng1/
[Elasticsearch]: https://www.elastic.co/
[MySQL]: https://www.mysql.com/
[AWS]: https://aws.amazon.com/

## Technical challenges

Since the question is specifically about Node.js and Angular, I shall focus
on the Angular aspect of our work, and some of the challenges it presented.

### Background

From the outset, the BBC wanted a very "shiny" web application that they
could sell to business clients. This was the reason we settled on using
Angular for its single-page-app abilities. Although initial load time would
be longer than a more "traditional" web app, this preloading should provide
a much snappier and slicker experience once the page had loaded.

### Routing

Our biggest challenge was routing and state persistence. From the get-go,
the BBC insisted on having "pretty" URLs to facilitate sharing. To complicate
matters, they also insisted on news articles appearing in a modal window,
so that users would not lose their place in long lists (eg search results).

This meant that - for example - if we have a search page set up with a large 
amount of state persisted in the query string (search term, filters, etc.),
then we would have to stash all of this query string state when transitioning
to a news article, and then restore that query string if the user does any one of:

- click the modal close button
- click outside the modal
- click "back" in their browser

To complicate matters further, users could navigate from one news article
directly to another, either through links in the article itself, or through
a paging feature - the ability to click "next" to go to the next news article
in their search. In either of these cases, the user still expects to be able
to close the modal and return to exactly where they were in their search.

On top of this, the modals could be opened on top of any one of several
underlying pages, which means that the path `/article/abc` could have any
unknown background state. Also, because that path only describes the modal
state, and not the background state, it's uncertain what the background state
should be when the user:

- opens the article in a new window/tab
- shares the page
- bookmarks the page and comes back to it later

It was when dealing with all of these corner cases that we realised that
Angular 1.4's built-in routing wouldn't suffice, and we moved to using
UI Router. UI Router offered us the ability to have sub-views: pages whose
views had a hierarchy, which was exactly what we wanted. We moved to having
the news article be a child view of the parent. This child view could have a
path completely independent of the parent view, but we could still navigate
up to the parent view without knowing the exact name of the parent view.

### Result paging

Another modal problem we faced was paging through search results. The BBC
wanted the functionality so that if a user is on a search page and opens a
news story, that they can press "next", and will be taken directly to the
next news story in the search.

Complicating this was the fact that they wanted this functionality on two
pages:

- the search page, which has paged results
- the news feed page, which has infinite scroll

Both of these pages had their own, different problems, and commonising the
two was trickier still.

Dealing with infinite scroll was relatively easy. Paging backwards through
articles was trivial, because the results are all in-memory (the assumption
being that a user would never scroll through enough articles to run out of
memory). Paging forward was fine for all loaded results. However, when we
reach the last loaded result, we had to also trigger a background load of
the next section of the feed, and then give the current news article a handle
to the next result (or hide the "next" button if none exists). This meant
greedy loading of results, but allowed us to keep lazy loading of the article
itself.

The search page was a little harder, since the BBC wanted users to be able
to keep paging through results, and have the search page underneath "keep
up" with the results. So if a user keeps clicking "next" until they're on a 
new set of results, then the search page underneath had to also switch to the
next page. The difficulty with this page was again on the final item on the page.
Once a user is on the final item, we need:

- to stay on our current page of search results
- ...but determine whether there is another search result
- ...and what that next search result is
- ...without transitioning the page underneath
- ...and it had to work when going "previous" too

In the end I solved this by performing a query for a single result when on the
final (or first) item on the page, to tell us the next item. This could have
also been solved by loading more items than were displayed on the page (ie
the previous and next articles), but getting a handle on the appropriate 
timestamps when paging would have been non-trivial, and the resulting mess in
the code probably wouldn't have been a good trade-off against another HTTP
request on an already AJAX-heavy site.

One final gotcha to do with this whole feature was to do with the browser "back"
button (the bane of SPAs, I quickly learnt). Since we were storing "next" and
"previous" article information in the state, rather than in the URL (because
we weren't allowed ugly URLs), we made it work such that a user could click
back arbitrarily, but if they ever skipped multiple history items (ie jumped
through their history), then the paging behaviour broke. We never solved this
problem, because it was judged that the cost would outweigh the benefits. If
we had tried to solve it, it probably would have looked something like keeping
our own "shadow" history, where we would have far more control over what we
store than the HTML5 `history` API (which can only store serializable objects).
