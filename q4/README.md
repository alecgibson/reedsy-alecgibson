# Question 4

## Question

Implement a simple directive (or component) in Angular 1.x with a template composed by two dropdowns and a text input, that requires with `ng-model` and stores the model value as a timestamp (`Date.prototype.getTime()`) but displays it as the day on the first dropdown (ex. '21'), the month abbreviated on second (ex, 'Aug') and the year on on the text input (ex. '2017'). The dropdowns and input are editable, and changes must reflect on model and vice-versa. Add test coverage as you see fit.

## Answer

### Installation

```bash
npm install
```

### Running

1. Start the server with `npm start`
2. Navigate to [`localhost:8080`](http://localhost:8080)
3. Open the developer console
4. Change the date
5. See the changed date in the console

### Tests

Tests will require that Chrome is installed.

```bash
npm test
```
