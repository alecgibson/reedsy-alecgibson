describe("Date Picker", function () {
  var $compile;
  var $rootScope;

  beforeEach(module("q4"));
  beforeEach(module("directives/datePicker/datePicker.html"));

  beforeEach(inject(function (_$compile_, _$rootScope_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  describe("with a model whose timestamp is 24th Sep 2017", function () {
    var element;
    var dateSelect;
    var monthSelect;
    var yearInput;

    beforeEach(function () {
      $rootScope.timestamp = new Date("2017-09-24").getTime();
      element = $compile("<date-picker ng-model=timestamp></date-picker>")($rootScope);
      $rootScope.$digest();

      dateSelect = element[0].querySelector(".js-date");
      monthSelect = element[0].querySelector(".js-month");
      yearInput = element[0].querySelector(".js-year");
    });

    it("displays the provided date", function () {
      var date = displayedOption(dateSelect);
      var month = displayedOption(monthSelect);
      var year = yearInput.value;

      expect(date).toEqual("24");
      expect(month).toEqual("Sep");
      expect(year).toEqual("2017");
    });

    it("updates the provided model with the selected date", function () {
      dateSelect.selectedIndex = 4;
      monthSelect.selectedIndex = 5;
      yearInput.value = 2016;

      var date = displayedOption(dateSelect);
      angular.element(dateSelect).triggerHandler('change');
      var month = displayedOption(monthSelect);
      angular.element(monthSelect).triggerHandler('change');
      var year = yearInput.value;
      angular.element(yearInput).triggerHandler('change');

      expect(date).toEqual("5");
      expect(month).toEqual("Jun");
      expect(year).toEqual("2016");

      expect($rootScope.timestamp).toEqual(new Date("2016-06-05T00:00:00").getTime());
    });

    it("updates the view if the date is changed programmatically", function () {
      $rootScope.timestamp = new Date("2015-04-04").getTime();
      $rootScope.$digest();

      var date = displayedOption(dateSelect);
      var month = displayedOption(monthSelect);
      var year = yearInput.value;

      expect(date).toEqual("4");
      expect(month).toEqual("Apr");
      expect(year).toEqual("2015");
    });

    it("sets the value as NaN for an invalid date", function () {
      dateSelect.selectedIndex = 30;
      monthSelect.selectedIndex = 1;
      yearInput.value = 2017;

      var date = displayedOption(dateSelect);
      angular.element(dateSelect).triggerHandler('change');
      var month = displayedOption(monthSelect);
      angular.element(monthSelect).triggerHandler('change');
      var year = yearInput.value;
      angular.element(yearInput).triggerHandler('change');

      expect(date).toEqual("31");
      expect(month).toEqual("Feb");
      expect(year).toEqual("2017");

      expect($rootScope.timestamp).toBeNaN();
    });

    it("applies an invalid class with an invalid date", function () {
      var wrapper = element.find('span');
      expect(wrapper.hasClass('invalid')).toEqual(false);

      $rootScope.timestamp = "foo";
      $rootScope.$digest();

      expect(wrapper.hasClass('invalid')).toEqual(true);
    });
  });

  function displayedOption(select) {
    return select[select.selectedIndex].text;
  }
});
