angular.module("q4")
  .directive("datePicker", [function() {
    return {
      restrict: "E",
      templateUrl: "directives/datePicker/datePicker.html",
      scope: {
        ngModel: "="
      },
      link: link
    };

    function link(scope) {
      var momentDate;

      initialise();

      function initialise() {
        initialiseScope();
        setUpDateOptions();
        setUpMonthOptions();
        initialiseWatch();
      }

      function initialiseScope() {
        scope.updateDate = updateDate;
        scope.dateIsInvalid = dateIsInvalid;
      }

      function initialiseWatch() {
        scope.$watch('ngModel', setViewDate);
      }

      function setViewDate(newValue, oldValue) {
        if (isNaN(newValue) || momentDate && newValue === oldValue) {
          return;
        }

        momentDate = moment(scope.ngModel);
        scope.date = momentDate.date();
        scope.month = scope.months[momentDate.month()];
        scope.year = momentDate.year();
      }

      function setUpDateOptions() {
        scope.dates = [];
        for (var date = 1; date <= 31; date++) {
          scope.dates.push(date);
        }
      }

      function setUpMonthOptions() {
        scope.months = [];
        for (var month = 0; month <= 11; month++) {
          scope.months.push({
            value: month,
            label: moment.monthsShort(month)
          });
        }
      }

      function updateDate() {
        var newDate = moment({
          year: scope.year,
          month: scope.month.value,
          date: scope.date
        });

        scope.ngModel = newDate.valueOf();
      }

      function dateIsInvalid() {
        return isNaN(scope.ngModel);
      }
    }
  }]);