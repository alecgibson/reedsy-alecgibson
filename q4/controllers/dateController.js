angular.module("q4")
  .controller("dateController", ["$scope", function ($scope) {
    $scope.timestamp = new Date().getTime();

    $scope.$watch('timestamp', function(newValue) {
      console.log("Time changed to " + newValue);
      console.log(new Date(newValue));
    });
  }]);
  