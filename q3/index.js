const express = require("express");
const app = express();
const JobQueue = require("./services/JobQueue");

const DocumentsController = require("./controllers/DocumentsController");

const PORT = 3000;
const queue = new JobQueue();

registerControllers();
startServer();

function registerControllers() {
  new DocumentsController(queue).register(app);
}

function startServer() {
  app.listen(PORT, () => {
    console.info(`Listening on localhost:${PORT}`);
  });
}
