class DocumentConversion {
  constructor(file) {
    this.file = file;
  }

  job() {
    return () => {
      if (this.isHtml()) {
        return this.convertHtml();
      } else if (this.isPdf()) {
        return this.convertPdf();
      } else {
        return Promise.reject("Invalid file type");
      }
    };
  }

  isHtml() {
    var htmlMatcher = /\.htm(l)?$/;
    return htmlMatcher.test(this.file.originalname.toLowerCase());
  }

  isPdf() {
    var pdfMatcher = /\.pdf$/;
    return pdfMatcher.test(this.file.originalname.toLowerCase());
  }

  convertHtml() {
    console.log(`Processed html ${this.file.originalname}`);
    return Promise.resolve();
  }

  convertPdf() {
    console.log(`Processed pdf ${this.file.originalname}`);
    return Promise.resolve();
  }
}

module.exports = DocumentConversion;
