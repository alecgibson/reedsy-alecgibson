class JobQueue {
  constructor() {
    this.promise = Promise.resolve();
    this.htmlQueue = [];
    this.pdfQueue = [];
  }

  pushHtml(job) {
    this.htmlQueue.push(job);
  }

  pushPdf(job) {
    this.pdfQueue.push(job);
  }

  start() {
    return this.processPdfQueue()
      .then(() => { return this.processHtmlQueue(); })
      .then(() => {
        if (this.htmlQueue.length || this.pdfQueue.length) {
          return this.start();
        }
      });
  }

  processHtmlQueue() {
    return this.process(this.htmlQueue)
      .then(() => { return this.process(this.htmlQueue); })
      .then(() => { return this.process(this.htmlQueue); })
      .then(() => { return this.process(this.htmlQueue); })
      .then(() => { return this.process(this.htmlQueue); });
  }

  processPdfQueue() {
    return this.process(this.pdfQueue);
  }

  process(queue) {
    const job = queue.shift();
    return job ? job() : Promise.resolve();
  }
}

module.exports = JobQueue;
