const express = require("express");
const upload = require("multer")({ dest: 'uploads/' });
const DocumentConversion = require("../services/DocumentConversion");

class DocumentsController {
  constructor(queue) {
    this.queue = queue;
  }

  register(app) {
    const router = express.Router();

    app.use("/documents", router);

    router.post('/', upload.single("document"), this.processUpload.bind(this));
  }

  processUpload(request, response) {
    const conversion = new DocumentConversion(request.file);

    if (conversion.isHtml()) {
      this.queue.pushHtml(conversion.job());
      this.queue.start();
      response.sendStatus(202);
    } else if (conversion.isPdf()) {
      this.queue.pushPdf(conversion.job());
      this.queue.start();
      response.sendStatus(202);
    } else {
      response.status(400).send("Invalid file type");
    }
  }
}

module.exports = DocumentsController;
