const expect = require("chai").expect;
const DocumentConversion = require("../../services/DocumentConversion");

describe("DocumentConversion", function () {
  describe("converting a .html", function () {
    it("successfully converts", function (done) {
      const conversion = new DocumentConversion({originalname: "document.html"});
      const job = conversion.job();
      job().then(done);
    });
  });

  describe("converting a .pdf", function () {
    it("successfully converts", function (done) {
      const conversion = new DocumentConversion({originalname: "document.pdf"});
      const job = conversion.job();
      job().then(done);
    });
  });

  describe("converting a .exe", function () {
    it("does not convert", function (done) {
      const conversion = new DocumentConversion({originalname: "malicious.exe"});
      const job = conversion.job();
      job().catch(() => { done(); });
    });
  });
});
