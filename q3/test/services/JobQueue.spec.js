const expect = require("chai").expect;
const JobQueue = require("../../services/JobQueue");

describe("JobQueue", function () {
  it("resolves HTML jobs in the order they were inserted", function (done) {
    const queue = new JobQueue();

    const results = [];

    queue.pushHtml(() => {
      results.push("foo");
      return Promise.resolve();
    });

    queue.pushHtml(() => {
      results.push("bar");
      return Promise.resolve();
    });

    queue.start()
      .then(() => {
        expect(results).to.deep.equal(["foo", "bar"]);
        done();
      });
  });

  it("alternates between PDF and HTML queues, with 5 HTML for every 1 PDF", function (done) {
    const queue = new JobQueue();
    
    const results = [];

    queue.pushPdf(job("pdf #1"));
    queue.pushPdf(job("pdf #2"));
    queue.pushHtml(job("html #3"));
    queue.pushHtml(job("html #4"));
    queue.pushHtml(job("html #5"));
    queue.pushHtml(job("html #6"));
    queue.pushHtml(job("html #7"));
    queue.pushHtml(job("html #8"));
    queue.pushPdf(job("pdf #9"));
    queue.pushHtml(job("html #10"));

    queue.start()
      .then(() => {
        expect(results).to.deep.equal([
          "pdf #1",
          "html #3",
          "html #4",
          "html #5",
          "html #6",
          "html #7",
          "pdf #2",
          "html #8",
          "html #10",
          "pdf #9"
        ]);
  
        done();
      });

    function job(name) {
      return () => {
        results.push(name);
        return Promise.resolve();
      };
    }
  });
});
