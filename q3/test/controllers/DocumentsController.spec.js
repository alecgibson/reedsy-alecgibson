const fs = require("fs-extra");
const supertest = require("supertest");
const expect = require("chai").expect;
const sinon = require("sinon");
const express = require("express");
const DocumentsController = require("../../controllers/DocumentsController");

describe("DocumentsController", function () {
  let request;
  let queue;

  beforeEach(function () {
    const app = express();
    queue = { 
      pushHtml: sinon.spy(),
      pushPdf: sinon.spy(),
      start: sinon.spy()
    };

    const documentsController = new DocumentsController(queue);
    documentsController.register(app);
    request = supertest(app);
  });

  after(function () {
    fs.removeSync("uploads/");
  });

  describe("uploading a .html", function () {
    it("returns a 202 response", function (done) {
      request.post("/documents")
        .attach("document", "test/fixtures/document.html")
        .expect(202, () => {
          sinon.assert.calledOnce(queue.pushHtml);
          sinon.assert.calledOnce(queue.start);
          done();
        });
    });
  });

  describe("uploading a .pdf", function () {
    it("returns a 202 response", function (done) {
      request.post("/documents")
        .attach("document", "test/fixtures/document.pdf")
        .expect(202, () => {
          sinon.assert.calledOnce(queue.pushPdf);
          sinon.assert.calledOnce(queue.start);
          done();
        });
    });
  });

  describe("uploading a .zip", function () {
    it("returns a 400 response", function (done) {
      request.post("/documents")
        .attach("document", "test/fixtures/malicious.zip")
        .expect(400, done);
    });
  });
});
