# Alec Gibson Reedsy Assignment

This repository contains answers to the [questions set by Reedsy][questions]. The
response to each question is in its own directory.

[questions]: https://github.com/reedsy/challenges/blob/master/node-fullstack.md
