angular.module("q5")
  .factory("Operation", [function () {
    function Operation() {
      this.steps = [];
      this.totalCursorMoves = 0;
    }

    Operation.move = function(numberOfCharacters) {
      return {move: numberOfCharacters};
    };

    Operation.insert = function(string) {
      return {insert: string};
    };

    Operation.delete = function(numberOfCharacters) {
      return {delete: numberOfCharacters};
    };

    Operation.compose = function() {
      var operation = new Operation();
      for (var i = 0; i < arguments.length; i++) {
        operation.compose(arguments[i]);
      }
      return operation;
    };

    Operation.prototype.push = function(step) {
      var stepKeys = Object.keys(step);

      if (Object.keys(step).length !== 1) {
        return;
      }

      var stepType = stepKeys[0];
      var newStep;

      switch(stepType) {
        case "move":
          var numberOfCharacters = step.move;
          numberOfCharacters -= this.totalCursorMoves;
          this.totalCursorMoves += numberOfCharacters;
          newStep = Operation.move(numberOfCharacters);
          break;
        case "insert":
          newStep = Operation.insert(step.insert);
          break;
        case "delete":
          newStep = Operation.delete(step.delete);
          break;
        default:
          throw "Invalid operation";
      }

      this.steps.push(newStep);
      return this;
    };

    Operation.prototype.move = function(numberOfCharacters) {
      return this.push(Operation.move(numberOfCharacters));
    };

    Operation.prototype.insert = function(string) {
      return this.push(Operation.insert(string));
    };

    Operation.prototype.delete = function(numberOfCharacters) {
      return this.push(Operation.delete(numberOfCharacters));
    };

    Operation.prototype.compose = function(operation) {
      operation.steps.forEach(function (step) {
        this.push(step);
      }.bind(this));
    };

    Operation.prototype.apply = function(string) {
      var cursor = 0;

      this.steps.forEach(function (step) {
        var stepType = Object.keys(step)[0];

        switch(stepType) {
          case "move":
            cursor += step.move;
            break;
          case "insert":
            string = string.substr(0, cursor) + step.insert + string.substr(cursor, string.length);
            cursor += step.insert.length;
            break;
          case "delete":
            string = string.substr(0, cursor) + string.substr(cursor + step.delete, string.length);
            break;
          default:
            throw "Invalid operation";
        }
      });

      return string;
    };

    return Operation;
  }]);
