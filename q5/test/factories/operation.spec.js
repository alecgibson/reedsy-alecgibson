describe("Operation", function () {
  var Operation;

  beforeEach(module("q5"));

  beforeEach(inject(function (_Operation_) {
    Operation = _Operation_;
  }));

  describe("creating a single operation", function () {
    it("creates an array of steps", function () {
      var operation = new Operation()
        .move(5)
        .insert("foo");

      expect(operation.steps).toEqual([
        {move: 5},
        {insert: "foo"}
      ]);
    });
  });

  describe("composing an operation onto an existing operation", function () {
    it("removes extra moves from the second operation", function () {
      var operation = new Operation()
        .move(1)
        .insert("foo");

      var otherOperation = new Operation()
        .move(6)
        .insert("bar");

      operation.compose(otherOperation);

      expect(operation.steps).toEqual([
        {move: 1},
        {insert: "foo"},
        {move: 5},
        {insert: "bar"}
      ]);
    });
  });

  describe("composing multiple operations together", function () {
    it("can combine two operations into a new operation", function () {
      var operation1 = new Operation()
        .move(1)
        .insert("foo");

      var operation2 = new Operation()
        .move(6)
        .delete(2);

      var combinedOperation = Operation.compose(operation1, operation2);

      expect(combinedOperation.steps).toEqual([
        {move: 1},
        {insert: "foo"},
        {move: 5},
        {delete: 2}
      ]);
    });

    it("does not mutate the original operations", function () {
      var operation1 = new Operation()
        .move(1)
        .insert("foo");

      var operation2 = new Operation()
        .move(6)
        .delete(2);

      Operation.compose(operation1, operation2);

      expect(operation2.steps).toEqual([
        {move: 6},
        {delete: 2}
      ]);
    });
  });

  describe("applying an operation to a string", function () {
    it("can insert a string", function () {
      var string = "The brown fox";

      var result = new Operation()
        .move(4)
        .insert("quick ")
        .apply(string);

      expect(result).toEqual("The quick brown fox");
    });

    it("does not mutate the original string", function () {
      var string = "The brown fox";

      new Operation()
        .move(4)
        .insert("quick ")
        .apply(string);

      expect(string).toEqual("The brown fox");
    });

    it("can delete", function () {
      var string = "The brown fox";

      var result = new Operation()
        .move(4)
        .delete(6)
        .apply(string);

      expect(result).toEqual("The fox");
    });

    it("can combine deletion and insertion", function () {
      var string = "The brown fox";

      var deletion = new Operation()
        .move(4)
        .delete(6);

      var insertion = new Operation()
        .move(4)
        .insert("quick ");

      var result = Operation.compose(deletion, insertion)
        .apply(string);

      expect(result).toEqual("The quick fox");
    });
  });
});
