# Question 2

## Question

Detail how would you persist in data/present a schema to store several versioned 
text-based documents. It should allow to:

- save a version representing a document state
- keep the versions list/document history for browsing
- browse a previous version and
- visualize the changes/diff between two versions.

Strive for storage size efficiency.

## Answer

I would propose a system that stores diffs of documents. This would give
an accurate change history of the document. Although it would be more computationally
difficult to reconstruct a particular document (especially the latest document),
this provides an efficient use of space, whilst maintaining the necessary detail
for restoring arbitrary versions, and viewing diffs between arbitrary versions.

If we were truly striving for space saving, we may also compress the values of the
diffs, although arguably this might have little effect if the changes are small,
for added complexity and computation.

### Proposed schema

```json
{
  "document_id": "string",
  "version": 1,
  "publication_date": "2017-09-23T00:00:00Z",
  "changes": [
    {
      "type": "string",
      "index": 1,
      "value": "string"
    }
  ]
}
```

Where: 
- `document_id` is an ID representing a single document. The ID is unique amongst
  documents, but not necessarily unique in the stored diffs, since a single
  document may have several diffs. 
- `version` is an integer that represents the version of the document (higher is
  newer). Together with `document_id`, these provide a unique key for the diff.
- `publication_date` is the timestamp at which the version was published.
- `changes` is an array of changes that must be made to the previous version to
  create this version.
- `changes.type` indicates whether the change is an addition or deletion (or 
  potentially a move, depending on the algorithm being used).
- `changes.index` is the index in the previous document at which the change needs
  to be applied.
- `changes.value` is the value being inserted or deleted

### Potential extras

On top of the proposed schema, the following fields may also be (individually)
considered.

```json
{
  "schema_version": "string",
  "diff_id": "string",
  "changes.end_index": 1
}
```

- `schema_version` is a string representing the version of this schema being used.
  This could be useful if the schema needs to change (especially in a breaking way).
- `diff_id` a truly unique ID for this diff, which isn't strictly necessary, but
  may be convenient. Striving for storage efficiency says we could probably skip it.
- `changes.length` is the length of the change. Strictly when applying a deletion, 
  we don't need to store the actual value of the deletion - just its length. We
  could save some space if we omitted this information at the cost of a bit more
  work when visualising diffs.
